<?php
ob_start();
session_start();
require_once('database.php');
$message = '';
$class='';
$id = $_GET['id'];
$res = $database->delete($id);
if($res){
    $message = "Successfully deleted data";
    $class="alert alert-success";
    $_SESSION['message'] = $message;
    $_SESSION['class'] = $class;
	header('location: index.php');
}else{
    $message = "Failed to Delete Record";
    $class="alert alert-danger";
    $_SESSION['message'] = $message;
    $_SESSION['class'] = $class;
	header('location: index.php');
}
?>
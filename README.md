# PHP Quick CRUD Oops

We need to follow the below steps to setup this application

# Step-1
 
 We need to create a new database in MysQL/PHPMYadmin
 
 # Step-2
 
 We need to execute this schema inside database:
 
--
-- Table structure for table `crud`
--
`CREATE TABLE `crud` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `age` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `crud`
--
`ALTER TABLE `crud`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `crud`
--
`ALTER TABLE `crud`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;`
  `
# Step-3

We need update the database connection parameter inside database.php

# Done....




